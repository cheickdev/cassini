//
//  ViewController.swift
//  Cassini
//
//  Created by Cheick Mahady SISSOKO on 02/01/2016.
//  Copyright © 2016 Telecom ParisTech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private struct Images {
        static let Earth = "Earth"
        static let Cassini = "Cassini"
        static let Saturn = "Saturn"
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let ivc = segue.destinationViewController as? ImageViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case Images.Earth:
                    ivc.imageURL = DemoURL.NASA.Earth
                    ivc.title = Images.Earth
                case Images.Cassini:
                    ivc.imageURL = DemoURL.NASA.Cassini
                    ivc.title = Images.Cassini
                case Images.Saturn:
                    ivc.imageURL = DemoURL.NASA.Saturn
                    ivc.title = Images.Saturn
                default: break
                }
            }
        }
    }
    
}

